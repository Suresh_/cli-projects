import urllib as urlib, urllib.request as request
import threading as thread, datetime, time
def checkstatus(timeperiod,site_url):
    tmp = True
    try:
        init_time = datetime.datetime.now().second
        tocallat = init_time+timeperiod
        while(tmp):
            stat = request.urlopen(site_url).getcode()
            if(stat is 200):
                print("Status code is 200, The site "+site_url+" is up and running.. :)")
            elif(stat is 404):
                print("Status code is 404, The site "+ site_url +" is not found.. :(")    
            while(datetime.datetime.now().second is not tocallat):
                time.sleep(1)
    except Exception as e:
        print(e)
print(".........Is the site up?.........")
get_choice = input("Select an option to proceed\n\t1.Check if the site is up\n\t2.Exit   ")
if(get_choice is "1"):
    get_url = input("What's the URL of the site that you wanna check? : ")
    get_waitime = int(input("Let me know the seconds to wait to check again: "))
    checkstatus(get_waitime,get_url)
elif(get_choice is "2"):
    exit()



